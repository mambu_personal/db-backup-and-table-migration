#!/bin/bash
BACKUP_DIR=backup
COPY_TABLE_DIR=copy

source ./.colors.sh

trap 'echo "${txtred}An error occurred. Exiting...${txtrst}"; exit 1;' ERR #handler_error

read -p "Enter username: " PGUSER
read -p "Enter your db name: " PGDATABASE
read -p "You want to backup your database entirely or just copy a table? (b/t) " TYPE

if [ "$TYPE" == "t" ]; then

    if [ -d "$COPY_TABLE_DIR" ]; then
        echo "${txtgrn}Folder exists, proceed with backup ${txtrst}"
    else
        echo "${txtylw}$COPY_TABLE_DIR folder does not exist, proceed with creation ${txtrst}"
        mkdir "$COPY_TABLE_DIR"
    fi

    read -p "Enter the table you want to copy: " TABLE
    pg_dump -a -U "$PGUSER" -t "$TABLE" -f "$COPY_TABLE_DIR/$TABLE.sql" "$PGDATABASE"
    echo "${txtcyn}Done ${txtrst}"

    read -p "Now you want to copy this table to another db? (y/n) " COPY

    if [ "$COPY" == "y" ]; then
        read -p "Host IP: " HOST_IP
        read -p "Db name: " REMOTE_DB_NAME
        read -p "Username: " REMOTE_DB_USERNAME
        echo "${txtgrn}Connecting to remote database and importing table. You will be prompted for the password.${txtrst}"
        psql -h "$HOST_IP" -d "$REMOTE_DB_NAME" -U "$REMOTE_DB_USERNAME" -f "$COPY_TABLE_DIR/$TABLE.sql"
        echo "RESULTS: "
        psql -h "$HOST_IP" -d "$REMOTE_DB_NAME" -U "$REMOTE_DB_USERNAME" -c "SELECT * FROM \"$TABLE\";"
    if [ $? -eq 0 ]; then
        echo "${txtgrn}Query executed successfully${txtrst}"
    else
        echo "${txtred}Failed to execute query${txtrst}"
    fi
    else
        echo "${txtblu}All Done${txtrst}"
    fi

elif [ "$TYPE" == "b" ]; then

    if [ -d "$BACKUP_DIR" ]; then
        echo "${txtgrn}Folder exists, proceed with backup ${txtrst}"
    else
        echo "${txtylw}$BACKUP_DIR folder does not exist, proceed with creation ${txtrst}"
        mkdir "$BACKUP_DIR"
    fi

    datestamp=$(date +'%Y-%m-%d')
    timestamp=$(date +'%H%M')
    pg_dump -U "$PGUSER" -d "$PGDATABASE" > "$BACKUP_DIR/$PGDATABASE"_"$datestamp"_"$timestamp".sql
    echo "${txtcyn}Done ${txtrst}"

else
    echo "${txtred}Invalid input, please try again ${txtrst}"
    exit 1
fi
