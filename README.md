This is a simple project written in shell script (.sh) designed for UNIX like OS that permit to decide if you want to backup your local database (at the moment postgresql) or copy a specific table for a migration on a new database avoiding waste of time and totally automated. You just need you hostname, port, db_name, username and password(See Special Requirements for mpre info). 

Commands:
- ./tables.sh  To execute the script

Base Requirements:
- UNIX-like OS (developed and tested on macOS)
- postgresql DBMS

Special Requirements:
If you own or have access to the remote database and want to completely automate your process of migration do these simple steps:
- Create or modify your .pgpass hidden file that contains credentials for auto-login so you don't have to insert password. => nano .pgpass
- Write your credentials in this format => [host_name]:[port]:[db_name]:[username][password]
- You're done!